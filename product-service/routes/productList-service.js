/**
 * @name hello-v1-api
 * @description This module packages the Hello API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const ServerResponse = require('fwsp-server-response');

let serverResponse = new ServerResponse();
express.response.sendError = function (err) {
  serverResponse.sendServerError(this, { result: { error: err } });
};
express.response.sendOk = function (result) {
  serverResponse.sendOk(this, { result });
};

let api = express.Router();
hydra.registerService();

/**
 * @api {get} /v1/products/ gets products list
 * @apiName products-service
 * @apiGroup User
 *
 * @apiVersion 0.0.1
 *
 *
 * @apiSuccess {[Json]} list of products.
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
            internet: { name: 'Quota', description: '10 GB', price: '100000 RP' },
            calls: { name: 'Calls', description: '200 mins', price: '50000 RP' },
            sms: { name: 'SMS', description: '100 SMS', price: '10000 RP' }
        }
 *
 * @apiError NoProductFound  No Products Found
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "NoProductsFound"
 *     }
 */

api.get('/', (req, res) => {
  var isAuthorized = true;
      isAuthorized = service.result.isAuthorized;
      if (isAuthorized) {
        console.log((200) + '' , {'message': 'user authorized' });

      } else {
        console.log((401) + '' , { "error": "Unauthorized" });
      }

      res.sendOk({
        internet: [
          { name: 'Quota', description: '10 GB', price: '100000 RP' },
          { name: 'Quota', description: '10 GB', price: '100000 RP' },
          { name: 'Quota', description: '10 GB', price: '100000 RP' }
        ],
        calls: [
          { name: 'Calls', description: '200 mins', price: '50000 RP' },
          { name: 'Calls', description: '200 mins', price: '50000 RP' },
          { name: 'Calls', description: '200 mins', price: '50000 RP' }
        ],
        sms: [
          { name: 'SMS', description: '100 SMS', price: '10000 RP' },
          { name: 'SMS', description: '100 SMS', price: '10000 RP' },
          { name: 'SMS', description: '100 SMS', price: '10000 RP' }
        ]
      });
  
});


let message = hydra.createUMFMessage({
  to: 'auth-service:[get]/v1/auth',
  from: 'product-service:/',
  body: {
    code: 1234
  }
});

module.exports = api;
