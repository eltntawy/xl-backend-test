/**
 * @name test-v1-api
 * @description This module packages the Test API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const ServerResponse = require('fwsp-server-response');

let serverResponse = new ServerResponse();
express.response.sendError = function (err) {
  serverResponse.sendServerError(this, { result: { error: err } });
};
express.response.sendOk = function (result) {
  serverResponse.sendOk(this, { result });
};

let api = express.Router();
/**
 * @api {post} /v1/auth/ Authenticates user
 * @apiName authentication-service
 * @apiGroup User
 *
 * @apiVersion 0.0.1
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {boolean} isAuthorized  returns if the user is authorized.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       {isAuthorized:true}
 *     }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */
api.post('/', (req, res) => {
  const code = req.body.code;

  console.log('-------------------------------------------');
  console.log('body:',req.body);
  console.log('params:',req.params);
  console.log('query:',req.query);
  console.log('code:',code);
  console.log('-------------------------------------------');

  if (code && code == 1234) {
    res.sendOk({ isAuthorized: true });
  } else {
    res.status(401).send({ isAuthorized: false });
  }
});

module.exports = api;
