# MyXl Mobile Backend

This project is for the XL backend serving My XL mobile app based on microservices architecture.

This project use docker-compose to orchestrate microservices and containers

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment link below for how to deploy the project on a live system.

### Prerequisites
Download Node js
https://nodejs.org/en/download/

Download docker 
https://www.docker.com/get-docker

### Installing
Run the below command to install all dependencies for each service

```sh
$ npm install
```
---

### Run on local machine
```sh
$ docker-compose build                      # build all containers
$ docker-compose up -d                      # build and run all containers
$ docker-compose down                       # stop all running containers
```

### scale up some service 
```sh
$ docker-compose scale <service_name>=3  
# ex
$ docker-compose scale product-service=5    # run 5 containers of product-services
```

##### Open hydra router page at  http://localhost:5353/
--- 

### Add new service

##### [important] to add new service you should add a container for this service in docker-compose.yml file

```docker
some-service: 
  build: <directory_name>
  links: 
    - <redis_container_name>:redis           # to like this service with redis container
  restart: always
```

### How to build docker image for specific service

#### build docker image for any services
```sh
$ cd <services-directory>
$ npm run docker build
```

### Deploy for production
#### coming soon !! :) 